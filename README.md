# k8s-cd-demo-app-a

## description

this setup shows how, a CI/CD pipeline could work, with a little bit of gitlab magic

### about this app

Demo app for Continous Delivery Workflow

returns '''foobar''' when triggered.

this app calls app-b for the '''bar''' part