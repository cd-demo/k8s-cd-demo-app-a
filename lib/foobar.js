const rq = require('request-promise')

function foobar() {
    return new Promise((resolve, reject) => {
        console.debug('Calling App B')
        rq.get('http://dang-app-b-staging.westeurope.cloudapp.azure.com:8082/bar')
        .then(result => {

        resolve({
            value: 'foo' + result
        })
        }).catch(err => {
            reject(err)
        })
    })
}

module.exports = foobar