const express = require('express')
const app = express()
const foobar = require('./lib/foobar')
const os = require('os')
 
const PORT = process.env.PORT || 3001

app.use('/', function (req, res, next) {
    console.log(`${req.method}: ${req.path} called`)
    next()
})

app.get('/foobar', function (req, res) {
  foobar().then(result => res.send(result))
    .catch(error => {
      res.status(500).send({
          message: 'Could not get foobar',
          reason: error.message
      })
  })
})



app.get('/host', function (req, res) {
  res.send({ host: os.hostname() })
})
 
app.listen(PORT)

console.info(`Server up and running on port ${PORT}`)